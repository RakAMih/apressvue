import Vue from 'vue'
import Vuex from 'vuex' 
import {API} from "../api/products.js"

Vue.use(Vuex)
export default new Vuex.Store({
    // state хранит данные
    state:{
        products:[],
        basket:[],
    },
    // Действия инициируют мутации, могут выполнять ассинхронные действия
    actions:{
        fetchProducts(){
            const divans = API.products
            this.products = divans
        },
    },
    // Мутации изменяют state
    mutations:{
        addBasketList(state, item){
            state.basket.push(item)
        },
        removeBasketList(state, id){
            state.basket.splice(id, 1)
        }
    },
    // getters вычисляет свойства state 
    getters:{
        listProduct(state){
            return state.products
        },
        basketListLength(state){
            return state.basket.length
        }
    },
})