import Vue from 'vue'
import App from './App.vue'
import store from './store/store'
import router from './router/router.js'
new Vue({
  router,
  store,
  el: '#app',
  render: h => h(App)
})
